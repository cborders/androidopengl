#include <jni.h>

#include "native/Render/Renderer.h"
#include "native/AssetLoader.h"

Renderer *renderer;
Moo::AssetLoader *assetLoader;

extern "C"
{
    JNIEXPORT void
    JNICALL
    Java_productions_moo_ctest_views_GameView_nativeInit(JNIEnv *env, jobject instance) {
        assetLoader = new Moo::AssetLoader(env);
        renderer = new Renderer(assetLoader);
        renderer->initGL();
    }

    JNIEXPORT void
    JNICALL
    Java_productions_moo_ctest_views_GameView_nativeResize(JNIEnv *env, jobject instance, jint width, jint height) {
        renderer->resize(width, height);
    }

    JNIEXPORT void
    JNICALL
    Java_productions_moo_ctest_views_GameView_nativeDraw(JNIEnv *env, jobject instance, jfloat deltaTime) {
        renderer->draw(deltaTime);
    }

	JNIEXPORT void
	JNICALL
	Java_productions_moo_ctest_views_GameView_nativeTouchDown(JNIEnv *env, jobject instance, jint pointerId, jfloat x, jfloat y) {
		renderer->touchDown(pointerId, x, y);
	}

	JNIEXPORT void
	JNICALL
	Java_productions_moo_ctest_views_GameView_nativeTouchMove(JNIEnv *env, jobject instance, jint pointerId, jfloat x, jfloat y) {
		renderer->touchMove(pointerId, x, y);
	}

	JNIEXPORT void
	JNICALL
	Java_productions_moo_ctest_views_GameView_nativeTouchUp(JNIEnv *env, jobject instance, jint pointerId, jfloat x, jfloat y) {
		renderer->touchUp(pointerId, x, y);
	}
}