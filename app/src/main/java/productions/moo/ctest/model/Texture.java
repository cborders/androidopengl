package productions.moo.ctest.model;

public class Texture
{
	public final int[] pixels;
	public final int width, height;

	public Texture(int[] pixels, int width, int height)
	{
		this.pixels = pixels;
		this.width = width;
		this.height = height;
	}
}
