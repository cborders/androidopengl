package productions.moo.ctest;

import android.app.Application;

public class OpenGLApplication extends Application
{
	private static OpenGLApplication _instance;

	public static OpenGLApplication getInstance ()
	{
		return _instance;
	}

	public OpenGLApplication ()
	{
		super();
		_instance = this;
	}
}
