package productions.moo.ctest.views;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GameView extends GLSurfaceView implements GLSurfaceView.Renderer, View.OnTouchListener
{
	private long _lastTimeStamp;

	public GameView (Context context)
	{
		this(context, null);
	}

	public GameView (Context context, AttributeSet attrs)
	{
		super(context, attrs);

		setEGLContextClientVersion(2);

		setOnTouchListener(this);

		setRenderer(this);
	}

	@Override
	public void onPause ()
	{
		super.onPause();
	}

	@Override
	public void onResume ()
	{
		super.onResume();
	}

	@Override
	protected void onAttachedToWindow ()
	{
		super.onAttachedToWindow();
	}

	@Override
	protected void onDetachedFromWindow ()
	{
		super.onDetachedFromWindow();
	}

	@Override
	public void onSurfaceCreated (GL10 gl10, EGLConfig eglConfig)
	{
		nativeInit();
	}

	@Override
	public void onSurfaceChanged (GL10 gl10, int width, int height)
	{
		nativeResize(width, height);
	}

	@Override
	public void onDrawFrame (GL10 gl10)
	{
		if(_lastTimeStamp == 0)
		{
			_lastTimeStamp = System.currentTimeMillis();
		}

		long now = System.currentTimeMillis();

		nativeDraw((now - _lastTimeStamp) / 1000f);

		_lastTimeStamp = now;
	}

	@Override
	public boolean onTouch (View v, MotionEvent event)
	{
		MotionEvent.PointerCoords coords = new MotionEvent.PointerCoords();
		for (int i = 0; i < event.getPointerCount(); i++)
		{
			int pointerId = event.getPointerId(i);
			event.getPointerCoords(i, coords);

			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
					nativeTouchDown(pointerId, coords.x, coords.y);
					break;
				case MotionEvent.ACTION_MOVE:
					nativeTouchMove(pointerId, coords.x, coords.y);
					break;
				case MotionEvent.ACTION_UP:
					nativeTouchUp(pointerId, coords.x, coords.y);
					break;
			}
		}

		return true;
	}

	private native void nativeInit ();
	private native void nativeResize (int width, int height);
	private native void nativeDraw (float deltaTime);

	private native void nativeTouchDown(int pointerId, float x, float y);
	private native void nativeTouchMove(int pointerId, float x, float y);
	private native void nativeTouchUp(int pointerId, float x, float y);
}
