package productions.moo.ctest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import productions.moo.ctest.views.GameView;

public class MainActivity extends AppCompatActivity
{
	static
	{
		System.loadLibrary("native-lib");
	}

	private GameView _gameView;

	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		_gameView = new GameView(this);
		setContentView(_gameView);
	}
}
