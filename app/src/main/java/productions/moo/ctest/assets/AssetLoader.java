package productions.moo.ctest.assets;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.util.Scanner;

import productions.moo.ctest.OpenGLApplication;
import productions.moo.ctest.model.Texture;

public class AssetLoader
{
	public static String readFile(String fileName)
	{
		Context context = OpenGLApplication.getInstance();
		AssetManager manager = context.getAssets();
		try
		{
			return readFileContents(fileName, manager);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static Texture loadTexture(String fileName, boolean hasAlpha)
	{
		Context context = OpenGLApplication.getInstance();
		AssetManager manager = context.getAssets();

		try
		{
			Bitmap image = BitmapFactory.decodeStream(manager.open(fileName));
			int width = image.getWidth();
			int height = image.getHeight();

			int[] pixels = new int[width * height];

			image.getPixels(pixels, 0, width, 0, 0, width, height);

			int bytesPerPixel = 3;
			if(hasAlpha) { bytesPerPixel = 4; }

			int[] colors = new int[width * height * bytesPerPixel];

			for(int i = 0; i < pixels.length; i ++)
			{
				int r = (pixels[i] >> 16) & 0xff;
				int g = (pixels[i] >> 8) & 0xff;
				int b = pixels[i] & 0xff;

				colors[i * bytesPerPixel + 0] = r;
				colors[i * bytesPerPixel + 1] = g;
				colors[i * bytesPerPixel + 2] = b;

				if(hasAlpha)
				{
					int alpha = (pixels[i] >> 24) & 0xFF;
					colors[i * bytesPerPixel + 3] = alpha;
				}
			}

			return new Texture(colors, width, height);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static String readFileContents (String fileName, AssetManager manager) throws IOException
	{
		Scanner fileInput = new Scanner(manager.open(fileName));
		StringBuilder fileContents = new StringBuilder();

		while (fileInput.hasNextLine())
		{
			fileContents.append(fileInput.nextLine());
		}

		return fileContents.toString();
	}
}
